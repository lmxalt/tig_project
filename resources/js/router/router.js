import { createRouter, createWebHistory } from 'vue-router';

import middlewarePipeline from './middlewrePipeline';

const home = () => import('../pages/home.vue');
const page404 = () => import('../pages/404.vue');

const routes = [
    {
        path: "/",
        name: "Home",
        component: home,
        meta: { title: 'Главная страница' },
    },
    {
        path: "/:pathMatch(.*)*",
        name: "NotFound",
        component: page404,
        meta: { title: '404'},
    },
];

const router = createRouter({
    routes,
    history: createWebHistory()
});

router.beforeEach((to, from, next) => {
    if(!to.meta.middleware) {
        return next()
    }
    const middleware = to.meta.middleware

    const context = {
        to,
        from,
        next,
        store
    }

    return middleware[0] ({
        ...context,
        next: middlewarePipeline(context, middleware, 1)
    })
})

export default router;