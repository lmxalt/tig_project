<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offer;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportOffers;

class XmlController extends Controller
{
    public function convertXml() 
    {
        $xml = simplexml_load_file('https://quarta-hunt.ru/bitrix/catalog_export/export_Ngq.xml');
        $offersPath = $xml->shop->offers->offer;
        $categoriesPath = $xml->shop->categories->category;
        foreach ($offersPath as $offer) {
            $offerCategoryId = (integer)$offer->categoryId;
            $groupCategories = $this->findParentsCategories($offerCategoryId, $categoriesPath);
                
            if (isset($groupCategories['category'])) {
                $mainCategory = $groupCategories['category'];
            }
            if (isset($groupCategories['sub_category'])) {
                $sub_category = $groupCategories['sub_category'];
            }
            if (isset($groupCategories['sub_sub_category'])) {
                $sub_sub_category = $groupCategories['sub_sub_category'];
            }
            $offer_id = $offer['id'];
            $category = $mainCategory;
            $sub_category = $sub_category;
            $sub_sub_category = $sub_sub_category;
            $sub_sub_category_id = $this->checkIfNull($offer->categoryId);
            $available = $offer['available'];
            $url = $this->checkIfNull($offer->url);
            $price = $this->checkIfNull($offer->price);
            $oldprice = $this->checkIfNull($offer->oldprice);
            $name = $this->checkIfNull($offer->name);
            $currency_id = $this->checkIfNull($offer->currency_id);
            $picture = $this->checkIfNull($offer->picture);
            $vendor = $this->checkIfNull($offer->vendor);
            $checkExistingRows = [ 
                'offer_id' => $offer_id,
                'category' => $category,
                'sub_category'  => $sub_category,
                'sub_sub_category'  => $sub_sub_category,
                'sub_sub_category_id' => $sub_sub_category_id,
                'available'  => $available,
                'url'  => $url,
                'price'  => $price,
                'oldprice'  => $oldprice,
                'name'  => $name,
                'currency_id'  => $currency_id,
                'picture'  => $picture,
                'vendor'  => $vendor,
            ];
            $existingOfferRow = Offer::where($checkExistingRows)->first();
            if (!$existingOfferRow) {
                $offerRow = Offer::create($checkExistingRows);
            }
        }
        $dataOffers = Offer::all();
        return Excel::download(new ExportOffers($dataOffers), 'offers.xlsx');
    }
    public function checkIfNull($value) 
    { 
        if ($value) {
            return $value;
        } else return Null;
    }
    public function checkIfIsset($value) 
    { 
        if (isset($value)) {
            return $value;
        } else return Null;
    }
    public function findParentsCategories($offerCategoryId, $categoriesPath) {
        $treeCategory = array();
        foreach ($categoriesPath as $category) {
            $categoryId = (integer)$category->attributes()->id;
            if ($offerCategoryId == $categoryId) {
                $currentCategory = $category;
                $treeCategory['sub_sub_category'] = $currentCategory;
            }
        }
        while ((integer)$currentCategory['parentId'] !== 0) {
            $currentParentId = (integer)$currentCategory['parentId'];
            foreach ($categoriesPath as $category) {
                $categoryId = (integer)$category->attributes()->id;
                if ($categoryId == $currentParentId) {
                    $parentCategory = $category;
                    if ((integer)$parentCategory->attributes()->parentId) {
                        $treeCategory['sub_category'] = $parentCategory;
                    } else $treeCategory['category'] = $parentCategory;
                    $currentCategory = $parentCategory;
                }
            }
        }
        return $treeCategory;
    }
    public function downloadExcel() 
    {
        $dataOffers = Offer::all();
        return Excel::download(new ExportOffers($dataOffers), 'offers.xlsx');
    }
}
