<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportOffers implements FromCollection, WithHeadings
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    public function headings(): array {
        return [
            'id',
            'offer_id',
            'available',
            'category',
            'sub_category',
            'sub_sub_category',
            'sub_sub_category_id',
            'url',
            'price',
            'oldprice',
            'name',
            'currency_id',
            'picture',
            'vendor',
            'created_at',
            'updated_at',
        ];
    }
    public function collection()
    {
        return collect($this->data);
    }
}