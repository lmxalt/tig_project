<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use HasFactory;

    protected $fillable = [
        'offer_id',
        'category',
        'sub_category',
        'sub_sub_category',
        'sub_sub_category_id',
        'available',
        'url',
        'price',
        'oldprice',
        'name',
        'currency_id',
        'picture',
        'vendor',
    ];
}
