## Как развернуть проект локально
Клонируем проект  
1. git clone https://gitlab.com/lmxalt/tig_project.git
  

Запускаем докер  
2. docker-compose up -d  
3. docker exec -it tig_project_app bash  

Устанавливаем composer  
4. composer install

Копируем .env  
5. cp .env.example .env  
5.1 exit

Далее используя vim или nano в .env исправляете db подключение  
5.2 vim .env

DB_HOST=db  
DB_DATABASE=tig_project_db  
DB_USERNAME=root  
DB_PASSWORD=root

Входите в app среду  
docker exec -it tig_project_app bash  
и генерируете миграции и ключ    
6. php artisan migrate  
7. php artisan key:generate

Открываете доступ в storage  
8. chmod 777 -R storage

Установка npm  
9. npm install  
10. npm run dev