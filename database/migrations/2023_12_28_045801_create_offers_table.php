<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('offer_id');
            $table->string('available');
            $table->string('category')->nullable();
            $table->string('sub_category')->nullable();
            $table->string('sub_sub_category')->nullable();
            $table->string('sub_sub_category_id');
            $table->string('url');
            $table->decimal('price', 10, 2);
            $table->decimal('oldprice', 10, 2)->nullable();
            $table->string('name');
            $table->string('currency_id')->nullable();
            $table->string('picture')->nullable();
            $table->string('vendor');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offers');
    }
};
